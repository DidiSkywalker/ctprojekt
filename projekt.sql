-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2018 at 08:33 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projekt`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` varchar(16) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` int(11) NOT NULL,
  `creator` varchar(255) NOT NULL,
  `image` varchar(500) NOT NULL,
  `category` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `price`, `creator`, `image`, `category`, `timestamp`) VALUES
('BGdAiXLnpRQk12dw', 'E', 'E', 42069, 'E', 'http://i0.kym-cdn.com/photos/images/newsfeed/001/365/843/65d.jpg', 'E', '2018-06-05 14:20:23'),
('ecsmGpUIfyHLT5nl', 'ich_iel', 'ich_iel', 10, 'u/DESIGNERGRINDER', 'https://i.redditmedia.com/0UjhPCQOkaFs7YgfFNaV15LCbtaawgcBRP60frDgbeo.jpg?s=f9f9c38c2b53f4d5835b4d5886508ff8', 'ich_iel', '2018-06-05 14:17:23'),
('GvRe5tHgZMtvNhsd', 'Pepe', 'REEEEEEEE', 666, 'ANONYMOUS', 'http://i0.kym-cdn.com/photos/images/facebook/000/862/065/0e9.jpg', '4chan', '2018-06-05 15:55:06'),
('MkkDtUnvbeZwvfd4', 'Olenna', 'freefolk meme', 5, 'u/JohnORourke99', 'https://i.redditmedia.com/pal3I44F4pLjEZqFFTnxDTn0VJsoU9dYFZ9YS5QPTPI.jpg?s=acdcf7dc7c5698b770173d14d990e717', 'freefolk', '2018-06-05 14:18:19'),
('Q1AQ3lutF0V2bDZg', 'sayonara', 'jimmy neutron srsly?', 69, 'u/guitar_mouth_guy', 'https://i.redditmedia.com/2olCLD89UFu8W1uEAtucZ95ITDTu5S_RH4DJNVORW0g.jpg?fit=crop&crop=faces%2Centropy&arh=2&w=640&s=4966820d57a331f7942ac65fdd8ba497', 'me_irl', '2018-06-05 14:19:49');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchaseNr` int(11) NOT NULL,
  `userId` varchar(16) NOT NULL,
  `itemId` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`purchaseNr`, `userId`, `itemId`) VALUES
(1, 'rcQ1Cmwf', 'GvRe5tHgZMtvNhsd'),
(2, 'rcQ1Cmwf', 'Q1AQ3lutF0V2bDZg'),
(3, 'rcQ1Cmwf', 'GvRe5tHgZMtvNhsd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(16) NOT NULL,
  `username` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(256) NOT NULL,
  `password_salt` varchar(16) NOT NULL,
  `joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin` tinyint(1) NOT NULL,
  `tokens` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password_hash`, `password_salt`, `joined`, `admin`, `tokens`) VALUES
('QU7lT8N6', 'localhost', 'local@host.de', '3469039383e6d3167b08a89a39fa038ac7bd8625fb27e5d6fe2fa83abc723a75', 'NlVoVnUttG11YkUg', '2018-06-05 02:11:54', 0, 0),
('rcQ1Cmwf', 'test', 'test@test.de', 'a8bce518c86ead71c731d696bc63bbde920e5b0f9ba233790a2ff3685c4cafcf', '4U10kcYdKQpi2d2i', '2018-06-05 18:32:33', 1, 99);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchaseNr`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `purchaseNr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
