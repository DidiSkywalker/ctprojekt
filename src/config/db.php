<?php
  class db extends PDO {
    private $dbhost = 'localhost';
    private $dbuser = 'root';
    private $dbpass = '';
    private $dbname = 'projekt';

    function __construct() {
      $mysql_connect_str = "mysql:host=$this->dbhost;dbname=$this->dbname";
      parent::__construct($mysql_connect_str, $this->dbuser, $this->dbpass);
      $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

  }