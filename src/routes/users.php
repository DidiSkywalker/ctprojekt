<?php
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/api/users', function (Request $req, Response $res) {
    $sql = "SELECT * FROM users";
    try {
        $db = new db();
        $stmt = $db->query($sql);
        $users = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($users);
    } catch (PDOException $ex) {
        echo '{"success":false,"err":{"message":' . $ex->getMessage() . '}}';
    }
});

$app->get('/api/user/{id}', function (Request $req, Response $res) {
    $id = $req->getAttribute('id');

    $sql = "SELECT * FROM users WHERE id=$id";
    try {
        $db = new db();
        $stmt = $db->query($sql);
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($user);
    } catch (PDOException $ex) {
        echo '{"success":false,"err":{"message":' . $ex->getMessage() . '}}';
    }
});

$app->post('/api/user/add', function (Request $req, Response $res) {
    $username = $req->getParam('username');
    $email = $req->getParam('email');
    $password = $req->getParam('password');

    if (!(isset($username) && isset($email) && isset($password))) {
        return $res->withStatus(400);
    }

    $id = generateUid();
    $salt = generateRandomString(16);
    $hash = hashPassword($password, $salt);

    $sql = "INSERT INTO users (id, username, email, password_hash, password_salt) VALUES(:id, :username,:email, :hash, :salt)";
    try {
        $db = new db();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':hash', $hash);
        $stmt->bindParam(':salt', $salt);
        $result = $stmt->execute();
        $db = null;
        echo '{"success":true, "message":"User added"}';
    } catch (PDOException $ex) {
        echo '{"success":false,"err":{"message":"' . $ex->getMessage() . '"}}';
    }
});

$app->post('/api/user/{id}/addtokens', function (Request $req, Response $res) {
    $id = $req->getAttribute('id');

    $sql = "UPDATE users SET tokens=tokens+100 WHERE id=:id";
    try {
        $db = new db();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $result = $stmt->execute();
        $db = null;
        echo '{"success":true, "message":"User added"}';
    } catch (PDOException $ex) {
        echo '{"success":false,"err":{"message":"' . $ex->getMessage() . '"}}';
    }
});
