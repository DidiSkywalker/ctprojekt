<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/{page}', function(Request $req, Response $res) {
  return $res->withRedirect('./pages/'.$req->getAttribute('page').'.php');
});
