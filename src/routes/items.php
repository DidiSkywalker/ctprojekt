<?php
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/api/items', function(Request $req, Response $res) {
  $sql = "SELECT * FROM items";
  try {
    $db = new db();
    $stmt = $db->query($sql);
    $items = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    echo json_encode($items);
  } catch(PDOException $ex) {
    echo json_encode($ex);
  }
});

$app->get('/api/items/filter', function(Request $req, Response $res) {
  
});


$app->get('/api/item/{id}', function(Request $req, Response $res) {
  $sql = "SELECT * FROM items WHERE id=:id";
  $id = $req->getAttribute('id');
  try {
    $db = new db();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $item = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    echo json_encode($item[0]);
  } catch(PDOException $ex) {
    echo json_encode($ex);
  }
});

$app->post('/api/cart/add/{id}', function(Request $req, Response $res) {
  if(!isset($_SESSION["cart"])) {
    $_SESSION["cart"] = array();
  }

  $sql = "SELECT * FROM items WHERE id=:id";
  $id = $req->getAttribute('id');
  try {
    $db = new db();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $item = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    $_SESSION["cart"][] = $item[0];
  } catch(PDOException $ex) {
    echo json_encode($ex);
  }
});


$app->post('/api/items/add', function(Request $req, Response $res) {
  $sql = "INSERT INTO items (id, name, description, price, creator, image, category) "
        ."VALUES(:id, :name, :description, :price, :creator, :image, :category)";

  $item = array();
  $item["id"] = generateItemUid();
  $item["name"] = $req->getParam('name');
  $item["description"] = $req->getParam('description');
  $item["price"] = $req->getParam('price');
  $item["creator"] = $req->getParam('creator');
  $item["image"] = $req->getParam('image');
  $item["category"] = $req->getParam('category');

  if(count($item) != 7) {
    return $res->withStatus(400);
  }

  try {
    $db = new db();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $item["id"]);
    $stmt->bindParam(':name', $item["name"]);
    $stmt->bindParam(':description', $item["description"]);
    $stmt->bindParam(':price', $item["price"]);
    $stmt->bindParam(':creator', $item["creator"]);
    $stmt->bindParam(':image', $item["image"]);
    $stmt->bindParam(':category', $item["category"]);
    $stmt->execute();
    $db = null;
    echo '{"success":true}';
  } catch(PDOException $ex) {
    echo json_encode($ex);
  }
});


$app->post('/api/cart/clear', function(Request $req, Response $res) {
  $_SESSION["cart"] = array();
});


$app->post('/api/checkout', function(Request $req, Response $res) {
  if(isset($_SESSION["user"]) && isset($_SESSION["cart"])) {
    $userId = $_SESSION["user"];
    $items = $_SESSION["cart"];
    $tokenSum = 0;
    foreach($items as $item) {
      $tokenSum += $item->price;
    }

    $sql = "SELECT * FROM users WHERE id='$userId'";
    try {
        $db = new db();
        $stmt = $db->query($sql);
        $user = $stmt->fetchAll(PDO::FETCH_OBJ)[0];
        if($user->tokens >= $tokenSum) {
          $sql = "UPDATE users SET tokens=tokens-$tokenSum WHERE id='$userId'";
          $stmt = $db->prepare($sql);
          $stmt->execute();

          $sql = "INSERT INTO purchases (userId, itemId) VALUES(:userId, :itemId)";
          $stmt = $db->prepare($sql);
          $stmt->bindParam(':userId', $userId);
          foreach($items as $item) {
            $stmt->bindParam(':itemId', $item->id);
            $stmt->execute();
          }
        } else {
          echo "Not enough tokens";
        }

        $db = null;
    } catch (PDOException $ex) {
        echo '{"success":false,"err":{"message":' . $ex->getMessage() . '}}';
    }
  }
});