<?php
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/auth/login', function (Request $req, Response $res) {
    $login = $req->getParam('login');
    $password = $req->getParam('password');

    if (!(isset($login) && isset($password))) {
        return $res->withStatus(400);
    }

    $sql = "SELECT id, password_hash, password_salt, admin FROM users WHERE username=:x OR email=:x;";

    try {
        $db = new db();

        $stmt = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute(array(':x' => $login));
        $results = $stmt->fetchAll(PDO::FETCH_OBJ);
        $result = $results[0];
        $hash = hashPassword($password, $result->password_salt);
        $uid = $result->id;
        $password_hash = $result->password_hash;
        if($hash == $password_hash) {
          $_SESSION["user"] = $uid;
          if($result->admin) {
              $_SESSION["admin"] = true;
          }
          echo '{"success":true,"id":"'.$uid.'"}';
        } else {
          echo '{"success":false}';
        }
        $db = null;
    } catch (PDOException $ex) {
        $result->status(500)->withJson(json_encode($ex));
        echo '{"success":false}';
    }
});

$app->get('/auth/logout', function (Request $req, Response $res) {
    if (isset($_SESSION["user"])) {
        $_SESSION["user"] = null;
        $_SESSION["admin"] = null;
    }
    return $res->withRedirect('/projekt/public/home');
});
