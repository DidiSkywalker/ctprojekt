<?php
function generateRandomString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

/**
 * Generates a random UID and guarantees it's not already used
 */
function generateUid()
{
    $uid = generateRandomString(8);
    $sql = "SELECT * FROM users WHERE id=$uid";
    try {
        $db = new db();
        $stmt = $db->query($sql);
        $res = $db->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return generateUid();
    } catch (PDOException $ex) {
        return $uid;
    }
}

function generateItemUid()
{
    $uid = generateRandomString(16);
    $sql = "SELECT * FROM items WHERE id=$uid";
    try {
        $db = new db();
        $stmt = $db->query($sql);
        $res = $db->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return generateUid();
    } catch (PDOException $ex) {
        return $uid;
    }
}

function request($url, $data, $method = "POST") {
    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => $method,
            'content' => http_build_query($data),
        ),
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;
}

function hashPassword($pass, $salt) {
    return hash('sha256', $pass.$salt);
}