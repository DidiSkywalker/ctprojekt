<?php include 'partials/header.php'?>

  <div id="checkout-list">
  <h3>No Items.</h3></div>

  <a id="clear-cart" href="#!" class="btn blue">Clear</a>
  <a id="buy" href="#!" class="btn blue">Buy</a>

  <br><br>

  <script>
    $.get('http://localhost/projekt/public/shoppingcartitems', res => {
      let elements = "";
      let items = JSON.parse(res);
      for(let item of items) {
        let card = "<div class='col s12 m7'> <div class='card horizontal'> <div class='card-image'> <img src='"+item.image+"'> </div> <div class='card-stacked'> <div class='card-content'> <h5>"+item.name+"</h5> <p>"+item.description+"</p> <p class='blue-text'><strong>"+item.price+" Tokens</strong></p> </div> </div> </div> </div>";
        elements += card;
      }
      if(elements != "") $('#checkout-list').html(elements);
    });

    $('#buy').click(()=> {
      $.post('http://localhost/projekt/public/api/checkout', res => {
        console.log(res);
        document.location.href = "http://localhost/projekt/public/dashboard";
        clearCart();
      });
    });
  </script>

<?php include 'partials/footer.php'?>