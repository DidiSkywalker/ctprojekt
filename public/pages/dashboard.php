<?php include 'partials/header.php'; include '../../src/config/db.php'; ?>

  <h1 class="center">DASHBOARD</h1>

  <ul class="collection">
  <?php 
    if(!isset($_SESSION["user"])) {
      echo "<script> document.location.href = 'http://localhost/projekt/public/login' </script>";
    }
    $id = $_SESSION["user"];

    $sql = "SELECT itemId FROM purchases WHERE userId='$id'";
    try {
      $db = new db();

      $stmt = $db->query($sql);
      $results = $stmt->fetchAll(PDO::FETCH_OBJ);
      // print_r($itemIds);
      $sql = "SELECT * FROM items WHERE id=:id";
      $stmt = $db->prepare($sql);
      foreach($results as $result) {
        $itemId = $result->itemId;
        $stmt->bindParam(':id', $itemId);
        $stmt->execute();
        $item = $stmt->fetch(PDO::FETCH_OBJ); ?>
          <li class="collection-item avatar">
            <img src="<?php echo $item->image ?>" alt="" class="circle">
            <span class="title"><?php echo $item->name ?></span>
            <p><?php echo $item->description ?>
            </p>
            <a href="#!" class="secondary-content"><i class="material-icons">file_download</i></a>
          </li>
      <?php
      }
      $db = null;
  } catch (PDOException $ex) {
      echo json_encode($ex);
  }
  ?>
  </ul>

<?php include 'partials/footer.php' ?>