<?php include 'partials/header.php'; include '../../src/config/db.php'; ?>

  <h1 class="center">PROFILE PAGE</h1>

  <?php
    if(!isset($_SESSION["user"])) {
      echo "<script> document.location.href = 'http://localhost/projekt/public/login' </script>";
    }
    $sql = "SELECT * FROM users WHERE id='".$_SESSION["user"]."'";

    try {
        $db = new db();

        $stmt = $db->query($sql);
        $user = $stmt->fetchAll(PDO::FETCH_OBJ)[0];
        echo "<h3>Username: ".$user->username."</h3>";
        echo "<h3>Email: ".$user->email."</h3>";
        echo "<h3>Tokens: ".$user->tokens."</h3>";
        $db = null;
    } catch (PDOException $ex) {
        echo json_encode($ex);
    }
  ?>

  <a href="#" id="add-tokens" class="btn blue">Add 100 Tokens</a>

  <script>
    $('#add-tokens').click(() => {
      $.post('http://localhost/projekt/public/api/user/<?php echo $user->id ?>/addtokens', res => {
        location.reload();
      });
    });
  </script>

<?php include 'partials/footer.php' ?>