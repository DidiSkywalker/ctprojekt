<?php
  include 'partials/header.php';
  ?>

  <?php 
    $result = file_get_contents("http://localhost/projekt/public/api/items");
    $items = json_decode($result);
    foreach($items as $item): ?>
      <div class="row valign-wrapper">
        <div class="col s6 offset-s3 valign">
          <div class="card">
            <div class="card-image">
              <?php echo '<img src="'.$item->image.'"/>' ?>
              <a id="<?php echo $item->id ?>" class="add-to-cart btn-floating halfway-fab waves-effect waves-light blue"><i class="material-icons">shopping_cart</i></a>
            </div>
            <div class="card-content">
              <span class="card-title blue-text"><strong><?php echo $item->price ?> Tokens</strong></span>
              <h5><?php echo $item->name ?></h5>
              <p><?php echo $item->description ?></p>
            </div>
          </div>
        </div>
      </div>
  <?php endforeach;
  ?>

  <script>
    
    $('.add-to-cart').click(function(){
      let id = $(this).attr('id');
      $.post('/projekt/public/api/cart/add/'+id);
    });
  </script>

<?php include 'partials/footer.php' ?>