<?php include 'partials/header.php'; ?>

  <h1>RESTful API Documentation</h1>

  <h5>POST /api/user/add</h5>
  <p>Pass <strong>username</strong>, <strong>email</strong> and <strong>password</strong> to register a new User.</p>

  <div class="divider"></div>

  <h5>GET /api/items</h5>
  <p>Returns a JSON array of item objects containing all items in the database.</p>

<?php include 'partials/footer.php' ?>