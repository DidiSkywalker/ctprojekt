  </div>
  </main>

  <footer class="page-footer blue">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Memeshop</h5>
          <p class="grey-text text-lighten-4">Browse, buy and download your favourite memes!</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Links</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="/projekt/public/about">About</a></li>
            <li><a class="grey-text text-lighten-3" href="/projekt/public/developer">Developer</a></li>
            <li><a class="grey-text text-lighten-3" href="http://bitbucket.org/DidiSkywalker/ctprojekt">Bitbucket</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      © 2018 Daniel und Corvin
      </div>
    </div>
  </footer>

  <!-- Materialize -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){
      $('.sidenav').sidenav();
      $('.dropdown-trigger').dropdown();
      $('.modal').modal();
      $('.tooltipped').tooltip();

      $('form').submit(function(){
        let id = $(this).attr('id');
        console.log("Form "+id+" called");
        $.post($(this).attr('action'), $(this).serialize(), function(response){
              // do something here on success
              console.log("Form "+id+" returned", response);
              if(formCallbacks[id]) {
                console.log("Calling callback of "+id);
                formCallbacks[id](response);
              }
        },'json');
        return false;
      });

      $('#shopping-cart-button').click(() => {
        $.get('http://localhost/projekt/public/shoppingcartitems', res => {
          let elements = "";
          let items = JSON.parse(res);
          for(let item of items) {
            let card = "<div class='col s12 m7'> <div class='card horizontal'> <div class='card-image'> <img src='"+item.image+"'> </div> <div class='card-stacked'> <div class='card-content'> <h5>"+item.name+"</h5> <p>"+item.description+"</p> <p class='blue-text'><strong>"+item.price+" Tokens</strong></p> </div> </div> </div> </div>";
            elements += card;
          }
          if(elements != "") $('#modal-content').html(elements);
        });
      });

      $('#clear-cart').click(() => {
        clearCart();
      });
    });
  </script>
</body>
</html>
