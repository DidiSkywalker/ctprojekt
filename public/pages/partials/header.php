<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
  <title>CT Projekt</title>
  <!-- jQuery -->
  <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script>
    let formCallbacks = {};
    function addFormCallback(id, callback) {
      formCallbacks[id] = callback;
    }
    function clearCart() {
      $.post('/projekt/public/api/cart/clear');
    }
  </script>
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }
  </style>
  <?php 
    session_start();
    if(!isset($_SESSION["cart"])) {
      $_SESSION["cart"] = array();
    }
  ?>
</head>
<body>
  <header>
  <nav class="blue">
    <div class="nav-wrapper container">
      <a href="/projekt/public/home" class="brand-logo">Home</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
      
        <?php
          if (isset($_SESSION["admin"]) && $_SESSION["admin"] == true): ?>
          <li><a href="/projekt/public/admin" class="tooltipped" data-position="bottom" data-tooltip="Admin"><i class="material-icons">settings</i></a></li>
        <?php endif; ?>
        <li><a id="shopping-cart-button" href="#shopping-cart" class="modal-trigger tooltipped" data-position="bottom" data-tooltip="Shopping Cart (<?php echo count($_SESSION["cart"]) ?> Items)"><i class="material-icons">shopping_cart</i></a></li>
        <?php
        if (!isset($_SESSION["user"])): ?>
          <li><a href="/projekt/public/login">Log In</a></li>
        <?php else: ?>
          <li><a href="/projekt/public/dashboard" class="tooltipped" data-position="bottom" data-tooltip="Dashboard"><i class="material-icons">dashboard</i></a></li>
          <li><a href="/projekt/public/profile" class="dropdown-trigger" data-target='profile-dropdown'><i class="material-icons">account_box</i></a></li>
        <?php endif; ?>
      </ul>
      <div id="shopping-cart" class="modal  black-text">
        <div id="modal-content">
          <h3>No Items.</h3>
        </div>
        <div class="modal-footer">
          <a id="clear-cart" href="#!" class="modal-close waves-effect waves-green btn-flat">Clear</a>
          <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
          <a href="/projekt/public/checkout" class="modal-close waves-effect waves-green btn-flat">Checkout</a>
        </div>
      </div>
      <ul id='profile-dropdown' class='dropdown-content'>
        <li><a href="/projekt/public/profile">Tokens</a></li>
        <li><a href="/projekt/public/auth/logout">Logout</a></li>
      </ul>
    </div>
  </nav>
  </header>

  <main>
  <div class="container">

  <?php
    if(isset($_GET["m"])): ?>
    <div class="center">
      <div class="card-panel green">
        <span class="white-text">
          <?php echo $_GET["m"]; ?>
        </span>
      </div>
    </div>
  <?php endif; ?>
  <?php
    if(isset($_GET["e"])): ?>
    <div class="center">
      <div class="card-panel red">
        <span class="white-text">
          <?php echo $_GET["e"]; ?>
        </span>
      </div>
    </div>
  <?php endif; ?>