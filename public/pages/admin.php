<?php include './partials/header.php' ?>

  <h1 class="center">ADMIN CONSOLE</h1>
  
  <h3>New Item</h3>

  <form id="new-item" action="/projekt/public/api/items/add" method="POST">
    <div class="row">
      <div class="col s6 input-field">
        <input type="text" name="name" id="name">
        <label for="name">name</label>
      </div>
      <div class="col s6 input-field">
        <input type="text" name="description" id="description">
        <label for="description">description</label>
      </div>
      <div class="col s6 input-field">
        <input type="text" name="price" id="price">
        <label for="price">price</label>
      </div>
      <div class="col s6 input-field">
        <input type="text" name="creator" id="creator">
        <label for="creator">creator</label>
      </div>
      <div class="col s6 input-field">
        <input type="text" name="image" id="image">
        <label for="image">image</label>
      </div>
      <div class="col s6 input-field">
        <input type="text" name="category" id="category">
        <label for="category">category</label>
      </div>
      <div class="col s3 m2 input-field">
        <input type="submit" class="btn">
      </div>
    </div>
  </form>

  <script>
    addFormCallback('new-item', res => {
      document.location.href = "?m=Item added!";
    });
  </script>

<?php include './partials/footer.php' ?>