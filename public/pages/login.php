<?php include 'partials/header.php'?>

  <h1 class="center">Login</h1>

  <form autocomplete="off" id="login-form" action="/projekt/public/auth/login" method="POST">
    <input autocomplete="false" name="hidden" type="text" style="display:none;">
    <div class="row center">
      <div class="col s12 m6 offset-m3">
        <div class="input-field">
          <input type="text" name="login" id="login">
          <label for="login">Email or Username</label>
        </div>
      </div>
    </div>
    <div class="row center">
      <div class="col s12 m6 offset-m3">
        <div class="input-field">
          <input type="password" name="password" id="password">
          <label for="password">Password</label>
        </div>
      </div>
    </div>
    <div class="row center">
      <div class="col s12 m6 offset-m3">
        <div class="input-field">
          <input type="submit" value="Login" class="btn blue">
        </div>
      </div>
    </div>
  </form>

  <div class="divider"></div>

  <h2 class="center">Register new User</h2>
  
  <form autocomplete="off" id="register-form" action="/projekt/public/api/user/add" method="POST">
    <input autocomplete="false" name="hidden" type="text" style="display:none;">
    <div class="row center">
      <div class="col s12 m6 offset-m3">
        <div class="input-field">
          <input type="text" name="username" id="registerusername">
          <label for="registerusername">Username</label>
        </div>
      </div>
    </div>
    <div class="row center">
      <div class="col s12 m6 offset-m3">
        <div class="input-field">
          <input type="text" name="email" id="registeremail">
          <label for="registeremail">Email</label>
        </div>
      </div>
    </div>
    <div class="row center">
      <div class="col s12 m6 offset-m3">
        <div class="input-field">
          <input type="password" name="password" id="registerpassword">
          <label for="registerpassword">Password</label>
        </div>
      </div>
    </div>
    <div class="row center">
      <div class="col s12 m6 offset-m3">
        <div class="input-field">
          <input type="submit" value="Register" class="btn blue">
        </div>
      </div>
    </div>
  </form>

  <script>
    addFormCallback('login-form', (res) => {
      console.log(res);
      if(res.success) {
        window.location.href = "/projekt/public/home";
      } else {
        window.location.href = "?e=Login failed";
      }
    })
    
    addFormCallback('register-form', (res) => {
      console.log(res);
      if(res.success) {
        window.location.href = "/projekt/public/home";
      } else {
        window.location.href = "?e=Register failed";
      }
    })
  </script>
<?php include 'partials/footer.php'?>